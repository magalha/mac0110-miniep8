using Test

function troca(v, i, j)
	aux = v[i]
	v[i] = v[j]
	v[j] = aux
end

function insercao(v)
	tam = length(v)
	for i in 2:tam
		j = i
		while j > 1
			if compareByValueAndSuit(v[j], v[j - 1]) 
				troca(v, j, j - 1)
			else
				break
			end
			j = j - 1
		end
	end
	return v
end

#FUNÇÃO AUXILIAR
function valorcarta(x)
#	↓ Valores Int dos Char [2, 3, 4, 5, 6, 7, 8, 9, J, Q, K, A] 
	cartasInt = [50, 51, 52, 53, 54, 55, 56, 57, 74, 81, 75, 65] 
	valor = [2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14]
	valorx = 0
	if length(x) == 3
		valorx = 10
	else
		for i in 1:12
			if Int(x[1]) == cartasInt[i]
				valorx = valor[i]
			end
		end
	end
	return valorx
end

#FUNÇÃO AUXILIAR
function valorcartacomnaipe(x)
#	↓ Valores Int dos Char [2, 3, 4, 5, 6, 7, 8, 9, J, Q, K, A] 
	cartasInt = [50, 51, 52, 53, 54, 55, 56, 57, 74, 81, 75, 65] 
	valorcarta = [2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14]
#	↓ Valores Int dos Char [♦, ♠, ♥, ♣] 
	naipesInt = [9830, 9824, 9829, 9827]
	valornaipe = [0, 13, 26, 49]

	valorx = 0
	valornaipex = 0

	for i in 1:4
		if Int(x[length(x)]) == naipesInt[i]
			valornaipex = valornaipe[i]
		end
	end

	if length(x) == 3
		valorx = 10
	else
		for i in 1:12
			if Int(x[1]) == cartasInt[i]
				valorx = valorcarta[i] 
			end
		end
	end
	return valorx + valornaipex
end

function compareByValue(x, y)
	return valorcarta(x) < valorcarta(y)
end

function compareByValueAndSuit(x, y)
	return valorcartacomnaipe(x) < valorcartacomnaipe(y)
end


@test compareByValue("2♠", "A♠") == true
@test compareByValue("K♥", "10♥") == false
@test compareByValue("10♠", "10♥") == false
@test compareByValueAndSuit("2♠", "A♠") == true
@test compareByValueAndSuit("K♥", "10♥") == false
@test compareByValueAndSuit("10♠", "10♥") == true
@test compareByValueAndSuit("A♠", "2♥") == true
@test insercao(["10♥", "10♦", "K♠", "A♠", "J♠", "A♠"]) == ["10♦", "J♠", "K♠", "A♠", "A♠", "10♥"]

println("Fim dos Testes")